#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include "resource.h"
#include "prochack.h"
#include <string>

#define DLL_EMBEDDED

extern std::wstring dllName;
extern std::wstring procName;
extern bool bInjected;

std::wstring DropResource();

void Inject(HWND hwnd);