#include <Windows.h>
#include <thread>
#include "injector.h"
#include "GHLoader.h"
#include "GUI.h"
#include <string>

HCURSOR idc_hand = LoadCursor(0, IDC_HAND);
HCURSOR idc_arrow = LoadCursor(0, IDC_ARROW);
HINSTANCE hInst;

bool bInjected = false;

//Customize Below//
int windowWidth = 370;
int windowHeight = 250;
int bgrColor[3] = { 34, 34, 34 };

std::wstring wndTitle = L"GuidedHacking.com DragnipurV2";

std::wstring procName = L"ac_client.exe";
std::wstring dllName = L"DragnipurV2.0.dll";
std::wstring linkurl = L"http://guidedhacking.com/";

std::wstring text1 = L"GuidedHacking.com Assault Cube Hack";
std::wstring text2 = L"Press Insert to Activate Menu";
std::wstring text3 = L"Hack Status:  ";
std::wstring text3False = L"Game Not Found";
std::wstring text3True = L"Injected";
std::wstring text4 = L"Brought to you by [GH]Rake";
// Customize Above //

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HBRUSH hBanner = 0;

WNDCLASSEX MakeBasicWindowClass(HINSTANCE hInstance)
{
	WNDCLASSEX wndclass
	{
		sizeof(WNDCLASSEX),							//cbSize
		CS_HREDRAW | CS_VREDRAW,					//style
		WndProc,									//lpfnWndProc
		0,											//cbClsExtra
		0,											//cbWndExtra
		hInstance,
		LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1)),	//hIcon
		LoadCursor(0, IDC_ARROW),							//hCursor
		CreateSolidBrush(RGB(bgrColor[0], bgrColor[1],bgrColor[2])), //hbrBackground
		0,											//lpszMenuName
		wndTitle.c_str(),									//lpszClassName
		LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1)) //hIconSm
	};
	RegisterClassEx(&wndclass);

	return wndclass;
}

HWND MakeLoaderWindow(HINSTANCE hInstance, int iCmdShow)
{
	HWND hwnd = CreateWindow(
		wndTitle.c_str(),					//lpClassName
		wndTitle.c_str(),					//lpWindowName
		WS_SYSMENU | WS_MINIMIZEBOX,//dwStyle
		600,						//X
		300,						//Y
		370,						//nWidth
		250,						//nHeight
		0,							//hWndParent
		0,							//hMenu
		hInstance,					//hInstance
		0);							//lpParam

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);
	return hwnd;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	hInst = hInstance;

	WNDCLASSEX wndclass = MakeBasicWindowClass(hInstance);

	HWND hwnd = MakeLoaderWindow(hInstance, iCmdShow);

	std::thread Injector(Inject, hwnd);
	Injector.detach();
	MSG msg = { 0 };

	while (GetMessage(&msg, 0, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

bool bOneTime = false;
guiObject windowBody;

void InitGuiObjs(HDC hdc, HWND hwnd)
{
	GetFont(hdc);

	windowBody = guiObject(hdc, nullptr);

	GetClientRect(hwnd, &windowBody.rect);

	//Create banner
	guiBitmap * guiBanner = new guiBitmap(MAKEINTRESOURCE(IDB_BITMAP2));
	windowBody.AddChild(guiBanner);

	//Create rect for text
	guiObject * textRows = new guiObject(hdc, &windowBody);
	textRows->rect.top = guiBanner->rect.bottom;
	windowBody.AddChild(textRows);

	//Create text row objects
	textRows->AddChild(new guiText(text1));
	textRows->AddChild(new guiText(text2));
	textRows->AddChild(new guiText(text3 + text3False));
	textRows->AddChild(new guiText(text4));

	//dynamically size and place inside parent object
	textRows->PlaceRows();

	bOneTime = true;
}

void OnPaint(HDC hdc, HWND hwnd)
{
	if (!bOneTime) InitGuiObjs(hdc, hwnd);

	windowBody.UpdateHDC(hdc);

	//lmao
	if (bInjected)
	{
		((guiText*)(windowBody.children[1]->children[2]))->text = text3 + text3True;
	}
	else
	{
		((guiText*)(windowBody.children[1]->children[2]))->text = text3 + text3False;
	}

	windowBody.Draw();
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	switch (message)
	{
	case WM_CREATE:
	{
		break;
	}

	case WM_MOUSEMOVE:
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		//Select banner rect
		rect.bottom = rect.top + 75;

		POINT pt = { LOWORD(lParam), HIWORD(lParam) };

		if (PtInRect(&rect, pt))
		{
			//Change cursor to hand clicker on banner
			SetCursor(idc_hand);
		}
		else
		{
			SetCursor(idc_arrow);
		}
		break;
	}

	case WM_LBUTTONDOWN:
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		//Select banner rect
		rect.bottom = rect.top + 75;

		POINT pt = { LOWORD(lParam), HIWORD(lParam) };

		if (PtInRect(&rect, pt))
		{//Open Link from banner
			ShellExecute(NULL, L"open", linkurl.c_str(), NULL, NULL, SW_SHOWNORMAL);
		}
		break;
	}

	case WM_DESTROY:
	{
		DeleteObject(hBanner);
		PostQuitMessage(0);
		break;
	}

	case WM_PAINT:
	{
		hdc = BeginPaint(hwnd, &ps);
		OnPaint(hdc, hwnd);
		EndPaint(hwnd, &ps);
		break;
	}

	default:
	{
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	}
	return 0;
}