#include "injector.h"

std::wstring DropResource()
{
	HRSRC hResource = FindResource(0, MAKEINTRESOURCE(IDR_RT_RCDATA1), L"RT_RCDATA");
	HGLOBAL hFileResource = LoadResource(0, hResource);
	LPVOID lpFile = LockResource(hFileResource);
	uintptr_t resourceSize = SizeofResource(0, hResource);

	wchar_t dropLocation[MAX_PATH] = { 0 };
	GetTempPath(MAX_PATH, dropLocation);

	wcsncat_s(dropLocation, dllName.c_str(), dllName.size());
	std::wstring wDropLocation = dropLocation;

	HANDLE hFile = CreateFile(dropLocation, FILE_ALL_ACCESS, FILE_SHARE_READ | FILE_SHARE_DELETE, 0, CREATE_ALWAYS, 0, 0);
	DWORD at;
	WriteFile(hFile, lpFile, resourceSize, &at, 0);
	CloseHandle(hFile);

	return wDropLocation;
}

void Inject(HWND hwnd)
{
	while (1)
	{
		DWORD pid = 0;

		if (GetPid(procName.c_str(), &pid))
		{
			if (!bInjected)
			{
				if (!GetModuleBase(dllName.c_str(), pid))
				{
					HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, 0, pid);
					if (proc && proc != INVALID_HANDLE_VALUE)
					{
						void* dllLoadAddr = VirtualAllocEx(proc, 0, MAX_PATH, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

#ifdef DLL_EMBEDDED
						std::wstring path = DropResource();
#else
						//Grab from temp path instead, if already dropped
						/*
						wchar_t path[MAX_PATH];
						GetTempPath(MAX_PATH, path);
						wcsncat_s(path, dllName, wcslen(dllName));
						*/
#endif // DLL_EMBEDDED

						WriteProcessMemory(proc, dllLoadAddr, path.c_str(), path.size() * sizeof(wchar_t), 0);

						if (CreateRemoteThread(proc, 0, 0, (LPTHREAD_START_ROUTINE)LoadLibrary, dllLoadAddr, 0, 0) != NULL)
						{
							bInjected = true;
						}
#ifdef EMBEDDED
						//Mark file for deletion
						CloseHandle(CreateFile(path, FILE_ALL_ACCESS, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_DELETE_ON_CLOSE, 0));
#endif
						CloseHandle(proc);
					}
					else CloseHandle(proc);
				}
				else bInjected = true;
			}
		}
		else bInjected = false;

		InvalidateRect(hwnd, 0, true);
		Sleep(500);
	}
}