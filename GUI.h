#pragma once
#include <windows.h>
#include <vector>
#include <string>

//Font stuff
TEXTMETRIC tm;
int fontHeight;
int fontWidth;
int guiObjectPadding = fontHeight;

void GetFont(HDC hdc)
{
	GetTextMetrics(hdc, &tm);
	fontHeight = tm.tmHeight;
	fontWidth = tm.tmAveCharWidth;
}

class guiObject
{
public:
	RECT rect;
	RECT padding = { 0 };
	guiObject * parent = nullptr;
	std::vector <guiObject*> children;
	HDC hdc;

	guiObject() {} //default constructor

	guiObject(HDC hdc, guiObject* parent) //merge with init?
	{
		if (parent)
		{
			rect = parent->rect;
		}
	}

	void AddChild(guiObject* obj)
	{
		obj->hdc = this->hdc;
		obj->parent = this;

		rect = this->rect;// here or in constructor?

		children.push_back(obj);
	}

	void PlaceRows2()
	{
		for (unsigned int i = 0; i < children.size(); i++)
		{
			guiObject * c = children[i];

			c->rect = this->rect;//

			//c->rect.top = this->rect.top + i * rowHeight;
			//c->rect.bottom = c->rect.top + rowHeight;
		}
	}

	void PlaceRows()
	{
		int rowHeight = (rect.bottom - rect.top) / children.size();

		for (unsigned int i = 0; i < children.size(); i++)
		{
			guiObject * c = children[i];
			c->rect = this->rect;
			c->rect.top = this->rect.top + i * rowHeight;
			c->rect.bottom = c->rect.top + rowHeight;
		}
	}

	void UpdateHDC(HDC hdc)
	{
		this->hdc = hdc;
		for (auto c : children)
		{
			c->hdc = hdc;
		}
	}

	virtual void Draw()
	{
		for (auto c : children)
		{
			c->Draw();
		}
	}

	bool IsChild()
	{
		if (parent != nullptr)
		{
			return true;
		}
		else return false;
	}

	~guiObject()
	{
		for (auto c : children)
		{
			delete c;
		}
	}
};

class guiText : public guiObject
{
public:
	std::wstring text;
	DWORD align = DT_CENTER;
	unsigned int stringHeight;
	unsigned int stringWidth;

	guiText() {} //default constructor

	guiText(std::wstring text)
	{
		this->text = text;
		stringHeight = fontHeight;
		stringWidth = fontWidth * text.size();
	}

	virtual void Draw()
	{
		SetTextColor(parent->hdc, RGB(225, 225, 225));
		SetBkMode(parent->hdc, TRANSPARENT);
		DrawText(parent->hdc, text.c_str(), -1, &rect, align);
	}
};

class guiBitmap : public guiObject
{
public:

	HBITMAP hBitmap;

	guiBitmap() {} //default constructor

	guiBitmap(LPWSTR location) //consctructor
	{
		hBitmap = (HBITMAP)LoadImage(GetModuleHandle(0), location, IMAGE_BITMAP, 0, 0, NULL);

		HGDIOBJ oldBitmap;
		BITMAP bitmap;
		HDC hdcMem = CreateCompatibleDC(hdc);
		oldBitmap = SelectObject(hdcMem, hBitmap);
		GetObject(hBitmap, sizeof(BITMAP), &bitmap);
		SelectObject(hdcMem, oldBitmap);
		DeleteDC(hdcMem);
		rect.bottom = rect.top + bitmap.bmHeight;
		rect.right = rect.left + bitmap.bmWidth;
	}

	void Draw()
	{
		HGDIOBJ oldBitmap;
		BITMAP bitmap;
		HDC hdcMem = CreateCompatibleDC(hdc);
		oldBitmap = SelectObject(hdcMem, hBitmap);

		GetObject(hBitmap, sizeof(BITMAP), &bitmap);
		BitBlt(hdc, rect.left, rect.top, bitmap.bmWidth, bitmap.bmHeight, hdcMem, 0, 0, SRCCOPY);

		SelectObject(hdcMem, oldBitmap);
		DeleteDC(hdcMem);
	}
};
