#pragma once
#include <windows.h>
#include <TlHelp32.h>
#include <string>

bool GetPid(const wchar_t* targetProcess, DWORD* procID);

DWORD GetModuleBase(const wchar_t* ModuleName, DWORD ProcessId);
