# GH Loader

This is a simple Win32, GDI loader/injector with the GH logo and some basic text output.  It takes an embedded DLL as a resource and writes it to disk and then injects using CreateRemoteThread.

Must be run as admin, runasAdmin is included in manifest file.

![loader screenshot](https://image.prntscrect.com/image/n4DfxyIESAaT96PYNFmnQQ.png)

## Usage

To release your hack using this loader, update the customizable strings, replace the DLL resource with the new DLL and compile.

## TODO

Manual Mapping

## Development History

Mambda sent me some loader code, Kilo made GUI and I mated them together with some additional goodies.

## Credits

Mambda, Kilo for initial codenz help

[Thread for Original Project](http://guidedhacking.com/showthread.php?7908)